import React from "react";
import "../Assets/css/style.scss";
import Footer from "../Components/Footer";
import Header from "../Components/Header";
import MainContent from "../Components/MainContent";

class Home extends React.Component {
  render() {
    return (
      <div id="page">
        <Header />
        <MainContent />
        <Footer />
      </div>
    );
  }
}

export default Home;
