import React from "react";
import AdBanner from "./Atomic Component/AdBanner";
import Events from "./Atomic Component/Events";
import HeadingImage from "../Assets/images/dolphins.jpg";
import ImgContent from "./Atomic Component/ImgContent";
import ImgSideContent from "./Atomic Component/ImgSideContent";
import Connect from "./Atomic Component/Connect";
import BottomImage from "../Assets/images/penguin2.jpg";
import NewsLetter from "./Atomic Component/NewsLetter";

class MainContent extends React.Component {
  state = {
    content: [
      "This website template has been",
      "Designed By Nexsoft Bootcamp for you",
      "If you're having problems editing this website template.",
      "then don't hesitate to ask for help on the Forum.",
      "Duis a est eget nunc pretium laoreets",
    ],
  };
  render() {
    return (
      <div id="content">
        <div id="featured">
          <h2>Meet our Animals</h2>
          <ul>
            <AdBanner />
          </ul>
        </div>
        <div class="section1">
          <h2>Events</h2>
          <ul id="article">
            <Events />
          </ul>
        </div>
        <div class="section2">
          <h2>Blog : Curabitur sodales</h2>
          <p>
            This website template has been designed by <b>nexsoft.co.id</b> for
            you.
          </p>
          <a href="#">
            <img src={HeadingImage} alt="" />
          </a>
          <ul>
            {this.state.content.map((item) => (
              <li>
                <p>{item}</p>
              </li>
            ))}
          </ul>
          <ImgContent />
          <ImgSideContent />
        </div>
        <div class="section3">
          <Connect />
          <NewsLetter />
          <img src={BottomImage} alt="" />
        </div>
      </div>
    );
  }
}
export default MainContent;
