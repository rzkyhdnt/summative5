import React from "react";
import NavItem from "./Atomic Component/NavItem";
import ImgFooter from "../Assets/images/animal-kingdom.jpg";

class Footer extends React.Component {
  state = {
    navItem: ["Home", "Tickets", "The Zoo", "Events", "Blog", "Gallery"],
    navContent: [
      "Live : Have fun in your visit",
      "Love : Donate for the animals",
      "Learn : Get to know the animals",
    ],
  };
  render() {
    return (
      <div id="footer">
        <div>
          <a href="#" class="logo">
            <img src={ImgFooter} alt="" />
          </a>
          <div>
            <p>Ellentesque habitant morbi tristique senectus et 0000</p>
            <span>285 067-39 282 / 5282 9273 999</span>
            <span>email@nexsoft.co.id</span>
          </div>
          <ul class="navigation">
            {this.state.navItem.map((item) => (
              <li>
                <NavItem navitem={item} />
              </li>
            ))}
          </ul>
          <ul>
            {this.state.navContent.map((item) => (
              <li>
                <NavItem navitem={item} />
              </li>
            ))}
          </ul>
          <p>Copyright &copy; 2011 - All Rights Reserved</p>
        </div>
      </div>
    );
  }
}

export default Footer;
