import React from "react";
import LionLogo from "../Assets/images/lion-family.jpg";
import Navigation from "../Components/Navigation";
import Logo from "./Atomic Component/Logo";

class Header extends React.Component {
  render() {
    return (
      <body>
        <div id="page">
          <div id="header">
            <a href="#" id="logo">
              <Logo />
            </a>
            <ul>
              <li class="first">
                <h2>
                  <a href="live.html">Live</a>
                </h2>
                <span>Have fun in your visit</span>
              </li>
              <li>
                <h2>
                  <a href="#">Love</a>
                </h2>
                <span>Donate for the animals</span>
              </li>
              <li>
                <h2>
                  <a href="#">Learn</a>
                </h2>
                <span>Get to know the animals</span>
              </li>
            </ul>
            <a href="#">Buy tickets / Check Events</a>
            <ul id="navigation">
              <li id="link1" class="selected">
                <a href="index.html">Home</a>
              </li>
              <Navigation />
            </ul>
            <img src={LionLogo} alt="" />
            <div>
              <h1>Special Events:</h1>
              <p>
                This website template has been designed by <a href="#">Nexsoft Bootcamp</a>
              </p>
            </div>
          </div>
        </div>
      </body>
    );
  }
}
export default Header;
