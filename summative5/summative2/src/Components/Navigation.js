import React from "react";
import NavItem from "./Atomic Component/NavItem";

class Navigation extends React.Component {
  state = {
    navItem: [
      "The Zoo",
      "Visitors Info",
      "Tickets",
      "Events",
      "Gallery",
      "Contact Us",
    ],
  };

  render() {
    const { navItem } = this.state;
    return (
      <>
        {navItem.map((item, index) => (
          <li id={"link" + (index+2)}>
            <NavItem navitem={item} />
          </li>
        ))}
      </>
    );
  }
}

export default Navigation;
