import React from "react";

class Events extends React.Component {
  state = {
    data: [],
  };

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/list/events");
    const body = await response.json();
    this.setState({ data: body });
  }

  render() {
    const { data } = this.state;
    return (
      <>
        {data.map((item) => (
          <li class="first">
            <a href="#">
              <span>{item.date}</span>
            </a>
            <p>
              {item.body}
            </p>
          </li>
        ))}
      </>
    );
  }
}

export default Events;
