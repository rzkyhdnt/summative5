import React from "react";

class ContentHeader extends React.Component {
  render() {
    return (
      <>
        <h2>
          <a href="">{this.props.title}</a>
        </h2>
        <span>{this.props.body}</span>
      </>
    );
  }
}

export default ContentHeader;
