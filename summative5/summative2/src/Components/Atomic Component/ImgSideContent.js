import React from "react";

class ImgSideContent extends React.Component {
  state = {
    data: [],
  };

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/list/content");
    const body = await response.json();
    this.setState({ data: body });
  }

  render() {
    const { data } = this.state;
    return (
      <div id="section2">
        <ul>
          {data.slice(-1).map((item) => (
            <li>
              <a href="#">
                <img src={item.imgurl} alt="" />
              </a>
              <h4>
                <a href="#">{item.title}</a>
              </h4>
              <p>{item.body}</p>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default ImgSideContent;
