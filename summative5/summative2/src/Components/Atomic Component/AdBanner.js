import React from "react";

class AdBanner extends React.Component {
  state = {
    adbanner: [],
  };

  async componentDidMount() {
    const response = await fetch("http://localhost:8080/list/images");
    const body = await response.json();
    this.setState({ adbanner: body });
  }

  render() {
    const { adbanner } = this.state;
    return (
      <>
        {adbanner.map((item) => (
          <li>
            <a href="#">
              <img src={item.imgurl} alt="" />
            </a>
            <a href="#">{item.name}</a>
          </li>
        ))}
      </>
    );
  }
}

export default AdBanner;
