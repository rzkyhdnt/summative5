import React from "react";

class NavItem extends React.Component {
  render() {
    return (
        <>
        <a href="#">{this.props.navitem}</a>
        </>
    );
  }
}

export default NavItem;
