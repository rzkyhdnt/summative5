import React from "react";

class Connect extends React.Component {
  render() {
    return (
      <>
        <h2>Connect</h2>
        <a href="#" id="email">
          Email Us
        </a>
        <a href="#" id="facebook">
          Facebook
        </a>
        <a href="#" id="twitter">
          Twitter
        </a>
      </>
    );
  }
}

export default Connect;
