import React from 'react'
import Logos from "../../Assets/images/logo.jpg";

class Logo extends React.Component{
    render(){
        return(
            <img src={Logos} alt="" />
        )
    }
}

export default Logo;