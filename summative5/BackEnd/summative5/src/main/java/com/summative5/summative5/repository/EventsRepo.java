package com.summative5.summative5.repository;

import com.summative5.summative5.model.Events;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EventsRepo extends JpaRepository<Events, Integer>{
    
}
