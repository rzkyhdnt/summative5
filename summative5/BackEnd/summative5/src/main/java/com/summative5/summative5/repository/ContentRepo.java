package com.summative5.summative5.repository;

import com.summative5.summative5.model.Content;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ContentRepo extends JpaRepository<Content, Integer> {
    
}
