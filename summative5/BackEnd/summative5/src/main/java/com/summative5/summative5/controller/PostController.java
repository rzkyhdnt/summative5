package com.summative5.summative5.controller;

import com.summative5.summative5.model.Content;
import com.summative5.summative5.model.Events;
import com.summative5.summative5.model.ImgURL;
import com.summative5.summative5.repository.ContentRepo;
import com.summative5.summative5.repository.EventsRepo;
import com.summative5.summative5.repository.ImgURLRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value = "/save")
public class PostController {
    @Autowired
    ContentRepo contentRepo;

    @Autowired
    EventsRepo eventsRepo;

    @Autowired
    ImgURLRepo imgURLRepo;

    @RequestMapping(value = "/content", consumes = "application/json")
    public Content saveContent(@RequestBody Content content){
        return contentRepo.save(content);
    }

    @RequestMapping(value = "/events", consumes = "application/json")
    public Events saveEvents(@RequestBody Events events){
        return eventsRepo.save(events);
    }

    @RequestMapping(value = "/images", consumes = "application/json")
    public ImgURL saveImgURL(@RequestBody ImgURL imgURL){
        return imgURLRepo.save(imgURL);
    }
}
