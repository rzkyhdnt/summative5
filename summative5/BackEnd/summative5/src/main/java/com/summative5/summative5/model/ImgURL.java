package com.summative5.summative5.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "imgbanner")
public class ImgURL {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String imgurl;
    private String name;

    public ImgURL(){

    }

    public ImgURL(int id, String imgurl, String name) {
        this.id = id;
        this.imgurl = imgurl;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
