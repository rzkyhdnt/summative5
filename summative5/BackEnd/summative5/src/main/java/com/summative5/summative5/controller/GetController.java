package com.summative5.summative5.controller;

import java.util.List;

import com.summative5.summative5.model.Content;
import com.summative5.summative5.model.Events;
import com.summative5.summative5.model.ImgURL;
import com.summative5.summative5.repository.ContentRepo;
import com.summative5.summative5.repository.EventsRepo;
import com.summative5.summative5.repository.ImgURLRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value = "/list")
public class GetController {
    @Autowired
    ContentRepo contentRepo;

    @Autowired
    EventsRepo eventsRepo;

    @Autowired
    ImgURLRepo imgURLRepo;

    @GetMapping("/content")
    public List<Content> contentList(){
        return contentRepo.findAll();
    }

    @GetMapping("/events")
    public List<Events> eventsList(){
        return eventsRepo.findAll();
    }

    @GetMapping("/images")
    public List<ImgURL> imgurlList(){
        return imgURLRepo.findAll();
    }
}
