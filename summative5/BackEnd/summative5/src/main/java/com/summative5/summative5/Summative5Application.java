package com.summative5.summative5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Summative5Application {

	public static void main(String[] args) {
		SpringApplication.run(Summative5Application.class, args);
	}

}
