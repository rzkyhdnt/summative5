package com.summative5.summative5.repository;

import com.summative5.summative5.model.ImgURL;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ImgURLRepo extends JpaRepository<ImgURL, Integer> {
    
}
